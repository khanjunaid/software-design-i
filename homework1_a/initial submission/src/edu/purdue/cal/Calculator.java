package edu.purdue.cal;

import java.util.Scanner;

public class Calculator {
  
  public float summation(float a, float b) {
    return a + b;
  }

  public float subtraction(float a, float b) {
    return a - b;
  }

  public float multiplication(float a, float b) {
    return a * b;
  }

  public float division(float a, float b) {
    return a / b;
  }


  /**
   * Main method for calculator
   * 
   * @param args Npo Arg
   */
  public static void main(String[] args) {
    Calculator c = new Calculator();
    while (true) {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter first number");
      float a = sc.nextFloat();
      System.out.println("Enter second number");
      float b = sc.nextFloat();
      System.out.println("Choose Operator");
      System.out.println(
          "+ for addtion     - for subtraction      * for multiplication     / for division");
      char ch = sc.next().charAt(0);
      switch (ch) {
        case '+':
          System.out.println(c.summation(a, b));
          break;
        case '-':
          System.out.println(c.subtraction(a, b));
          break;
        case '*':
          System.out.println(c.multiplication(a, b));
          break;
        case '/':
          System.out.println(c.division(a, b));
          break;
        default:
          break;
      }
    }
  }
}
