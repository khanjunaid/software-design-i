package edu.purdue.cal.excep;

public class ExceptionMessage {

  public static final String Operand_Type_Error = "Please type the correct operand type";
  public static final String Operator_Error = "Please type the correct operator";
  public static final String Input_Error = "Please provide the correct input";

}
