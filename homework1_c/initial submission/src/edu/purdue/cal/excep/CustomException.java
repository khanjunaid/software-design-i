package edu.purdue.cal.excep;

public class CustomException extends Exception {


  public CustomException(String s) {
    super(s);
  }
}
