package edu.purdue.cal.impl;

import edu.purdue.cal.Operation;
import edu.purdue.cal.model.CalModel;

public class Addition implements Operation {

  @Override
  public double calculateResult(CalModel model) {
    // TODO Auto-generated method stub
    return model.getFirstOperand() + model.getSecondOperand();
  }

}
