package edu.purdue.cal.ui;

import edu.purdue.cal.common.Operators;
import edu.purdue.cal.excep.CustomException;
import edu.purdue.cal.excep.ExceptionMessage;
import edu.purdue.cal.model.CalModel;

import java.util.Scanner;

public abstract class UserInterface {

  public abstract void displayUserInterface();

  /**
   * @author Junaid Khan
   * @return CalModel Class 
   * @throws CustomException Custom Exception
   */
  public CalModel getUserInputData() throws CustomException {

    // TODO Auto-generated method stub
    System.out.println();
    CalModel model = new CalModel();


    Scanner sc = new Scanner(System.in);
    String userInput = sc.next();

    if (userInput.contains(Operators.ADDITION.symbol())) {
      model = populateData(userInput, Operators.ADDITION.symbol());

    } else if (userInput.contains(Operators.SUBTRACTION.symbol())) {
      model = populateData(userInput, Operators.SUBTRACTION.symbol());

    } else if (userInput.contains(Operators.MULTIPLICATION.symbol())) {
      model = populateData(userInput, Operators.MULTIPLICATION.symbol());

    } else if (userInput.contains(Operators.DIVISION.symbol())) {
      model = populateData(userInput, Operators.DIVISION.symbol());

    } else {
      throw new CustomException(ExceptionMessage.Operand_Type_Error);
    }
    return model;

  }

  private CalModel populateData(String userInput, String operator) throws CustomException {
    CalModel model = new CalModel();
    String[] x = userInput.split("\\" + operator);

    try {
      model.setFirstOperand(Double.parseDouble(x[0]));
      model.setSecondOperand(Double.parseDouble(x[1]));
      model.setOperator(operator);

    } catch (ArrayIndexOutOfBoundsException e) {
      // TODO: handle exception
      throw new CustomException(ExceptionMessage.Input_Error);
    } catch (Exception e) {
      // TODO: handle exception
      throw new CustomException(ExceptionMessage.Operand_Type_Error);
    }
    return model;
  }



}
