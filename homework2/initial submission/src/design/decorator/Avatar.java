package design.decorator;

public interface Avatar {

  String item();
  
  String decorateWith();
}
