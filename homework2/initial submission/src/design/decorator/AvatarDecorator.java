package design.decorator;

public abstract class AvatarDecorator implements Avatar {

  protected Avatar avatarDecorator;
  
  
  public AvatarDecorator(Avatar avatarDecorator) {
    super();
    this.avatarDecorator = avatarDecorator;
  }
  
  public String item() {
    return avatarDecorator.item();
  }
  

}
