package design.decorator;

public class Sweater extends AvatarDecorator {

  public Sweater(Avatar avatarDecorator) {
    super(avatarDecorator);
    // TODO Auto-generated constructor stub
  }

  @Override
  public String item() {
    // TODO Auto-generated method stub
    return super.item() + decorateWith();
  }


  public String decorateWith() {
    return "Sweater ";
  }

}
