package design.decorator;

import java.util.Scanner;

public class Main {

  /**
   * Method to show cosmetic items
   */
  public void showCosmetics() {
    System.out.println("1.  Jacket");
    System.out.println("2.  T-shirt");
    System.out.println("3.  Sweater");
    System.out.println("4.  Jeans");
    System.out.println("5.  Shorts");
    System.out.println("6.  Tie");
    System.out.println("7.  Neckless");
    System.out.println("8.  Earrings");
    System.out.println("9.  Sunglasses");
    System.out.println("10. Leather Shoes");
    System.out.println("11. High Heels");
    System.out.println("12. Running Shoes");
  }


  /**
   * Main Method to Start the Program
   * 
   * @param args String Array of arguments
   */
  public static void main(String[] args) {

    Main main = new Main();

    System.out.println("Please select your character ");
    System.out.println("1. Male");
    System.out.println("2. Female");

    Avatar avatar = null;

    Scanner sc = new Scanner(System.in);
    int chrctr = sc.nextInt();

    switch (chrctr) {
      case 1:
        avatar = new Male();
        break;

      case 2:
        avatar = new Female();
        break;
      default:
        break;
    }



    System.out.print("You have selected the ");
    System.out.println(avatar.decorateWith());

    boolean isExit = false;

    Avatar decorateAvatar = avatar;

    while (!isExit) {


      System.out.println("Please select a cosmetic for your character:");
      System.out.println("(Type 'exit' to finist)");
      main.showCosmetics();


      Scanner sc1 = new Scanner(System.in);
      String str1 = sc1.nextLine();
      int opt = 0;


      if (str1.contains("exit")) {
        break;
      } else {
        opt = Integer.parseInt(str1);
      }



      switch (opt) {
        case 1:
          decorateAvatar = new Jacket(decorateAvatar);
          break;
        case 2:
          decorateAvatar = new TShirt(decorateAvatar);
          break;
        case 3:
          decorateAvatar = new Sweater(decorateAvatar);
          break;
        case 4:
          decorateAvatar = new Jeans(decorateAvatar);
          break;
        case 5:
          decorateAvatar = new Shorts(decorateAvatar);
          break;
        case 6:
          decorateAvatar = new Tie(decorateAvatar);
          break;
        case 7:
          decorateAvatar = new Neckless(decorateAvatar);
          break;
        case 8:
          decorateAvatar = new Earrings(decorateAvatar);
          break;
        case 9:
          decorateAvatar = new Sunglasses(decorateAvatar);
          break;
        case 10:
          decorateAvatar = new LeatherShoes(decorateAvatar);
          break;
        case 11:
          decorateAvatar = new HighHeels(decorateAvatar);
          break;
        case 12:
          decorateAvatar = new RunningShoes(decorateAvatar);
          break;
        default:
          break;
      }

      System.out
          .println("You have selected " + decorateAvatar.decorateWith() + "for your character");

    }


    System.out.println("Your Character has the following items");
    System.out.println(decorateAvatar.item());


  }
}


