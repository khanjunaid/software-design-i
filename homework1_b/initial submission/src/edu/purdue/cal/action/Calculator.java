package edu.purdue.cal.action;

import edu.purdue.cal.Operation;
import edu.purdue.cal.impl.Addition;
import edu.purdue.cal.impl.Division;
import edu.purdue.cal.impl.Multiplication;
import edu.purdue.cal.impl.Subtraction;
import edu.purdue.cal.model.CalModel;
import java.util.HashMap;
import java.util.Map;


public class Calculator {

  static Map<Character, Operation> opMap;

  static {
    opMap = new HashMap<>();
    opMap.put('+', new Addition());
    opMap.put('-', new Subtraction());
    opMap.put('*', new Multiplication());
    opMap.put('/', new Division());
  }

  /**
   * Method to perform calculation.
   * @author Junaid Khan
   * @param model for getting operand and operator
   * @return double value
   */
  public double makeCalculation(CalModel model) {

    Operation op = opMap.get(model.getOperator());
    return op.calculateResult(model);

  }


}
