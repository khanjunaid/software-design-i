package edu.purdue.cal.action;

import edu.purdue.cal.model.CalModel;
import java.util.Scanner;

public class MainClass {

  /**
   * Main Method for the calculator.
   * 
   * @author Junaid Khan
   * @param args no aruguments to pass
   */
  public static void main(String[] args) {

    CalModel model = new CalModel();
    Calculator cal = new Calculator();
    Scanner sc = new Scanner(System.in);
    while (true) {

      boolean firstOperand = true;
      boolean secondOperand = true;
      boolean operator = true;
      // double
      while (firstOperand) {
        System.out.println("Enter first Operand");
        String a = sc.next();
        try {
          model.setFirstOperand(Double.parseDouble(a));
          firstOperand = false;
        } catch (Exception e) {
          // TODO: handle exception
          System.out.println("Please enter digits for first Operand");
        }
      }

      while (secondOperand) {
        System.out.println("Enter Second Operand");
        String a = sc.next();
        try {
          model.setSecondOperand(Double.parseDouble(a));
          secondOperand = false;
        } catch (Exception e) {
          // TODO: handle exception
          System.out.println("Please enter digits for second operand");
        }
      }

      while (operator) {
        System.out.println("Choose Operator");
        System.out.println(
            "+ for addtion     - for subtraction      * for multiplication     / for division");

        String a = sc.next();
        if (a.length() > 1 && !Calculator.opMap.containsKey(a)) {
          System.out.println("Please choose the right operator");
        } else {
          model.setOperator(a.charAt(0));
          operator = false;
        }
      }


      System.out.println(cal.makeCalculation(model));

    }
  }

}
