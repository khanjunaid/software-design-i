package edu.purdue.cal;

import edu.purdue.cal.model.CalModel;

public interface Operation {
  public double calculateResult(CalModel model);
}
