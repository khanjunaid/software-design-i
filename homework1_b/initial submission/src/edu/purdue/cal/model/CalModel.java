package edu.purdue.cal.model;

public class CalModel {

  private double firstOperand;
  private double secondOperand;
  private char operator;
  
  
  public double getFirstOperand() {
    return firstOperand;
  }
  
  public void setFirstOperand(double firstOperand) {
    this.firstOperand = firstOperand;
  }
  
  public double getSecondOperand() {
    return secondOperand;
  }
  
  public void setSecondOperand(double secondOperand) {
    this.secondOperand = secondOperand;
  }
  
  public char getOperator() {
    return operator;
  }
  
  public void setOperator(char operator) {
    this.operator = operator;
  }
  
}
