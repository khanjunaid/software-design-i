package edu.purdue.cal.action;

import edu.purdue.cal.Operation;
import edu.purdue.cal.OperationContext;
import edu.purdue.cal.excep.CustomException;
import edu.purdue.cal.model.CalModel;
import edu.purdue.cal.operator.factory.OperatorFactory;


public class CalculatorStrategy {


  /**
   * Method to perform calculation.
   * 
   * @author Junaid Khan
   * @param model for getting operand and operator
   * @return double value
   * @throws CustomException throws custom exception
   */
  public double makeCalculation(CalModel model) throws CustomException {
    OperationContext context =
        new OperationContext(OperatorFactory.buildOperator(model.getOperator()));
    return context.executeOperation(model);

  }


}
