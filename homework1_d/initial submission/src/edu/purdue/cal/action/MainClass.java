package edu.purdue.cal.action;

import edu.purdue.cal.excep.CustomException;
import edu.purdue.cal.model.CalModel;
import edu.purdue.cal.ui.UserInterface;
import edu.purdue.cal.ui.UserInterfaceContext;
import edu.purdue.cal.ui.impl.CommandLineInterface;
import edu.purdue.cal.ui.impl.GraphicUserInterface;
import java.util.Scanner;

public class MainClass {

  /**
   * Main Method for the calculator.
   * 
   * @author Junaid Khan
   * @param args no aruguments to pass
   * @throws CustomException Throws Custom Exception
   */
  public static void main(String[] args) throws CustomException {
    CalculatorStrategy cal = new CalculatorStrategy();
    CalModel model = null;



    while (true) {
      System.out.println("Please Type 1 for GUI Calculator");
      System.out.println("Please Type 2 for CLI Calculator");
      UserInterfaceContext uiContext = null;
      Scanner sc = new Scanner(System.in);

      char ch = sc.next().charAt(0);
      switch (ch) {
        case '1':
          uiContext = new UserInterfaceContext(new GraphicUserInterface());
          break;
          
        case '2':
          uiContext = new UserInterfaceContext(new CommandLineInterface());
          break;
          
        default:
          System.out.println("Please type the correct option ");

      }

      if (uiContext != null) {
        try {
          uiContext.display();
          model = uiContext.getData();
          System.out.println("=" + cal.makeCalculation(model));
        } catch (Exception e) {
          // TODO: handle exception
          System.out.println(e.getMessage());
          System.out.println();
        }
      }


    }


  }

}
