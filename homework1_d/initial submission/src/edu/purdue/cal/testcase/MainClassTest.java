package edu.purdue.cal.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import edu.purdue.cal.action.CalculatorStrategy;
import edu.purdue.cal.common.Operators;
import edu.purdue.cal.excep.CustomException;
import edu.purdue.cal.model.CalModel;
import edu.purdue.cal.ui.UserInterface;
import org.junit.jupiter.api.Test;


class MainClassTest {


  @Test
  void testAddOperation() throws CustomException {


    CalModel model = new CalModel();

    model.setFirstOperand(10);
    model.setSecondOperand(10);
    model.setOperator(Operators.ADDITION.symbol());

    CalculatorStrategy cal = new CalculatorStrategy();

    assertEquals(20, cal.makeCalculation(model));


  }

  @Test
  void testSubtraction() throws CustomException {

    CalModel model = new CalModel();

    model.setFirstOperand(10);
    model.setSecondOperand(10);
    model.setOperator(Operators.SUBTRACTION.symbol());
    CalculatorStrategy cal = new CalculatorStrategy();
    assertEquals(0, cal.makeCalculation(model));
  }

  @Test
  void testMultiply() throws CustomException {

    CalModel model = new CalModel();

    model.setFirstOperand(10);
    model.setSecondOperand(10);
    model.setOperator(Operators.MULTIPLICATION.symbol());
    CalculatorStrategy cal = new CalculatorStrategy();
    assertEquals(100, cal.makeCalculation(model));
  }

  @Test
  void testDivision() throws CustomException {

    CalModel model = new CalModel();

    model.setFirstOperand(10);
    model.setSecondOperand(10);
    model.setOperator(Operators.DIVISION.symbol());
    CalculatorStrategy cal = new CalculatorStrategy();
    assertEquals(1, cal.makeCalculation(model));
  }
}
