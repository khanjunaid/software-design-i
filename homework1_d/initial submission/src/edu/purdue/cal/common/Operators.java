package edu.purdue.cal.common;

public enum Operators {

  ADDITION {
    @Override
    public String symbol() {
      return "+";
    }
  },
  SUBTRACTION {
    @Override
    public String symbol() {
      return "-";
    }
  },
  MULTIPLICATION {
    @Override
    public String symbol() {
      return "*";
    }
  },
  DIVISION {
    @Override
    public String symbol() {
      return "/";
    }
  };



  public abstract String symbol();

}
