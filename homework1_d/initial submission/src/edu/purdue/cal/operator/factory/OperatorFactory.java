package edu.purdue.cal.operator.factory;

import edu.purdue.cal.Operation;
import edu.purdue.cal.excep.CustomException;
import edu.purdue.cal.excep.ExceptionMessage;
import edu.purdue.cal.impl.Addition;
import edu.purdue.cal.impl.Division;
import edu.purdue.cal.impl.Multiplication;
import edu.purdue.cal.impl.Subtraction;

public class OperatorFactory {

  /**
   * @author Junaid Khan Method is used to give operator objects at run time
   * @input takes String
   * @return Operation Class
   * @throws CustomException throws custom exception
   */
  public static Operation buildOperator(String op) throws CustomException {
    Operation operation = null;


    switch (op) {
      case "+":
        operation = new Addition();
        break;

      case "-":
        operation = new Subtraction();
        break;

      case "*":
        operation = new Multiplication();
        break;

      case "/":
        operation = new Division();
        break;
      default:
        throw new CustomException(ExceptionMessage.Operand_Type_Error);
    }

    return operation;
  }


}
