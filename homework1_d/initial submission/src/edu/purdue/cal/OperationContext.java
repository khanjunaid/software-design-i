package edu.purdue.cal;

import edu.purdue.cal.model.CalModel;

public class OperationContext {

  private Operation operation;
  
  public OperationContext(Operation operation) {
    this.operation = operation;
  }
  
  public double executeOperation(CalModel model) {
    return operation.calculateResult(model);
  }
  
}
