package edu.purdue.cal.ui;

import edu.purdue.cal.excep.CustomException;
import edu.purdue.cal.model.CalModel;

public class UserInterfaceContext {

  private UserInterface userInterface;
  
  public UserInterfaceContext(UserInterface userInterface) {
    this.userInterface = userInterface;
  }
  
  
  public void display() {
    userInterface.displayUserInterface();
  }
  
  public CalModel getData() throws CustomException {
    return userInterface.getUserInputData();
  } 
  
}
